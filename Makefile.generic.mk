.PHONY: generic-build-hello
generic-build-hello:
	gcc -o hello source/gitlab-ci-basics/main.c

.PHONY: hello_mpi_exe
hello_mpi_exe:
		mpicc -g  source/gitlab-quickstart/hello_mpi.c -o hello_mpi_exe

.PHONY: hello_mpi_omp_exe
hello_mpi_omp_exe:
		mpicc -g -fopenmp source/gitlab-quickstart/hello_mpi_omp.c -o hello_mpi_omp_exe