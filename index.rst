Welcome to the ECP CI Documentation
===================================

.. image:: docs/files/ecp_logo.png
  :scale: 25%
  :align: right

**The DOE Exascale Computing Project's (ECP) mission:**

  * *Develop exascale-ready applications and solutions that address currently
    intractable problems of strategic importance and national interest.*

  * *Create and deploy an expanded and vertically integrated software stack on
    DOE HPC pre-exascale and exascale systems.*

  * *Deliver US HPC vendor technology advances and deploy ECP products to DOE
    HPC pre-exascale and exascale systems.*

  * *Deliver exascale simulation and data science innovations and solutions to
    national problems that enhance US economic competitiveness, improve our
    quality of life, and strengthen our national security.*

The `ECP Software Ecosystem <https://exascale-qa.ornl.gov/research/#application>`_
is crucial to reach exascale computing capabilities.
A Continuous Integration (CI) testing solution was identified as a critical
need to allow application and software technology projects to test across HPC
architectures and software environments.

.. warning::

  Documentation is currently under development, please be aware that changes
  to structure and contents will occur.

.. toctree::
   :caption: Contents
   :maxdepth: 2

   docs/introduction.rst
   docs/ci-users.rst
   docs/examples.rst
   docs/admin.rst
