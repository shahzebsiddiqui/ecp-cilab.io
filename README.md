# ECP-CI GitLab Pages

Documentation efforts for the Exascale Compute Project CI effort.

## Locally build documentation

* Install [Python](https://www.python.org/downloads/) and [pip](https://pip.pypa.io/en/stable/installing/)
* Change directories to your local checkout

  ```shell
  cd <local repository checkout>
  ```

* Install required dependencies

  ```shell
  pip install -r requirements.txt
  ```

* Build command

  ```shell
  make html
  ```

### Results

* Generated in `_build/`
* Index found at `_build/html/index.html` and be opened in a local browser.

## Contributing

We actively welcome contributions in all forms as we work towards not just improving
documentation readability but expanding the overhaul content.

The core of the documentation efforts are built with [Sphinx](sphinx-doc.org). If
you are unfamiliar with it or reStructedText there are a several resources we
advised reviewing:

* [reStructuredText Primer](http://docutils.sourceforge.net/docs/user/rst/quickref.html)
* [Sphinx Documentation](http://www.sphinx-doc.org/en/stable/contents.html)
* [rst-cheatsheet](https://github.com/ralsina/rst-cheatsheet/blob/master/rst-cheatsheet.rst)

### Style Checker

In addition to ensuring that you are able to locally build your documentation we
utilize [doc8](https://pypi.org/project/doc8) style checker. Simply `pip install doc8`
and from the project directory:

  ```console
  $ doc8
  Scanning...
  Validating...
  ========
  Total files scanned = 32
  ```
