.. _ci-introduction:

Introduction
============

.. _ECP: https://www.exascaleproject.org
.. _GitLab: https://about.gitlab.com
.. _GitHub: https://github.com

`What is Continuous Integration (CI) testing? <https://bssw.io/items/what-is-continuous-integration-testing>`_

ECP_'s CI resources are based upon `CI functionality <https://about.gitlab.com/product/continuous-integration>`_
of GitLab_ but enhanced/leveraged in such a way as to best support the
requirements of HPC centers.

This section is for people already familiar with GitHub_ and GitHub_'s
continuous integration services and wanting to know more about GitLab_
and GitLab_'s continuous integration services and what the differences are
between the two.

If your project's *home* is as a GitHub_ project...

To use GitLab_ CI services, you will need to do several things

* Import or mirror your project to a GitLab_ instance

  * Then, configure your project in GitLab_ to *mirror* (e.g. stay in sync
    with its GitHub_ origin)

* Add a ``.gitlab-ci.yml`` file in the root directory of your project
* Configure one or more *runners*

Within the context of GitLab, the notion of a
`runner <https://docs.gitlab.com/runner/>`_ is a fundamental
aspect of the design. A *runner* is responsible for actually running the jobs
that perform CI related operations.

Enhancements
------------

The ECP Continuous Integration (CI) project is developing an infrastructure
to allow software projects to perform CI across high performance computing
(HPC) machines at the different lab facilities. The goal is to provide a
mechanism for rapid feedback on building and running test suites for the
various software teams, while also utilizing non-standard, heterogeneous
machine architectures, software stacks, and environments found across distinct
computing facilities. Supporting these goals necessitated a number of
enhancements be made to associated GitLab software products.

Setuid
~~~~~~

A core enhancement made to the `GitLab runner <https://docs.gitlab.com/runner/>`_
as part of the ECP CI project was
the introduction of setuid. A traditional runner would execute all CI jobs
under a single user account. By allowing for a setuid configuration we can
first verify the GitLab user’s local account has the authority to run on the
resource. After being verified the job then executed securely under that local
user, ensuring the observation of all local permissions.

In addition to supporting a mapping between GitLab server and local account,
enhancements have provided the ability to leverage service accounts. This
enhancement specifically allows Gitlab runners at the sites to execute jobs
as a local site, shared project account which in turn follows all existing
local site requirements, rules, and project/accounting info.  Once enabled by
the runner administrator a verification process can be established to ensure
all service account related usage policies are enforced.

With these enhancements the goal was not just to execute CI jobs under user’s
local account, it was to provide runner administrators with news mechanisms
to control and audit the process.

Batch Executor
~~~~~~~~~~~~~~

In addition to the setuid enhancements there has been the introduction of a
new `executor <https://docs.gitlab.com/runner/executors/>`_ type. The batch
executor runs user defined scripts at the job level by submitting to the
underlying scheduling system with each CI job equating to a single scheduler
submission. It is similar in functionality to the existing shell executor as
it also supports Bash scripts and is capable of leveraging the setuid
functionality. At the moment only Cobalt (`qsub`), Slurm (`sbatch`), and
LFS (`bsub`) are supported.

Federation
~~~~~~~~~~

In addition to the setuid functionality introduced to the runner the ECP CI
solution has added the concept of a federated CI job to both the runner and
server. Used within the context of the ECP CI project, federation describes a
special trust relationship between facility hardware environments (compute
resources, runner, and authentication) and a centralized GitLab
(hosted by `OSTI <https://www.osti.gov/>`).

The federation model arose out of need to address running CI jobs across
different facilities each maintaining their own authentication and user
management solutions.  Federation allows GitLab server and runners to perform
user authentication and verification respectively, using a sites’
Identity Provider.  The CI server is extended to allow authentication with
multiple DOE facility IdPs and to pass the relevant authentication information
to federated runners for potential further verification. In conjunction
with the setuid enhancements this works to ensure all jobs are executed under
the correct user permissions. This is based on utilizing the central GitLab
(OSTI) as a hub for CI runners to sites hosting test resources. This requires
mirroring project repositories to the central GitLab hub.

CI Use Models
-------------

.. image:: files/use-models.png
  :scale: 35%
  :align: center

Local Model
-----------

DOE sites can leverage the ECP CI enhancements made to the GitLab runner in
local environments. Supporting HPC workflows locally will require deployment
of the enhanced runner with setuid and batch capabilities. However, it is
expected that sites run the traditional upstream GitLab server in this local
model. The setuid enhancements (discussed above) allow jobs to execute as a
specified user account, while the batch executor works with commonly used site
scheduling platforms to submit and monitor jobs.

Local sites can deploy an environment consisting of a local Gitlab server and
setuid enabled runner.  Supporting a local site deployment comes with the
limitation of being only available for a single site - there is no concept of
supporting multiple identities or cross-site jobs without the federation
enhancements. It may however have more test resources available to runners than
the federated model may have. It is also a requirement that the account
management for the GitLab server and compute resources be handled through the
same system.Thus ensuring that a GitLab user equates directly to the same local
account on the HPC system.

Federated Model
---------------
.. image:: files/federation.png
  :scale: 80%
  :align: right

A federated deployment of Gitlab leverages the centralized Gitlab server
(hosted by OSTI) and federated runners deployed at the local sites.
Sites are required to provide a local IDP solution for handling their
site auth with OSTI Gitlab and federated runners.  When a federated job
(mirrored repository) executes it first checks for valid auth information(user
info. from site IDP) for the targeted runner site.  If the server determines
that a user and job are valid to run, it sends the job and user info to the
federated site runner.
The federated site runner can then perform an additional check for user, token,
job, project information and allow or reject a job from running at their local
site based on the job information received.

Within the scope of the ECP CI project, federation means to satisfy the local
site requirements(accounts, allocations, etc..) for jobs passed down from
the centralized server hosted off-site.
