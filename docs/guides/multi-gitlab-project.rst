Multiple GitLab Project CI Structure
====================================

When using GitLab's `repository mirroring <https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html>`_
functionality you will undoubtedly end up in a case where you will be
required to manage vastly different pipelines depending on the instance.
These examples are meant to support provide guidance on some advised
methods to handle such a case.

Single YAML File
----------------

It is possible to mange your pipelines for multiple GitLab instances
through a single ``.gitlab-ci.yml`` file. This can be seen to
great success implemented by the **GAMESS** team and their use of
GitLab `rules <https://docs.gitlab.com/ee/ci/yaml/#rules-clauses>`_
and examining the ``CI_SERVER_HOST``
`predefined variable <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_.

We can use the same general logic in an example pipeline
that will target two GitLab instances at different facilities:

    .. code-block:: yaml

      .rules-anl
        rules:
        - if: '$CI_SERVER_HOST == "gitlab.anl.gov"'
          when: always

      .rules-ornl
        rules:
        - if: '$CI_SERVER_HOST == "gitlab.ornl.gov"'
          when: always

      global-job:
        script:
          - test

      anl-job:
        extends: .rules-anl
        script:
          - test-anl

      ornl-job:
        extends: .rules-ornl
        script:
          - test-ornl

As you may imagine from reading the example any jobs with the `rules`_
defined will only run when CI is triggered from the associated server.

Multiple YAML files
-------------------

Depending on how you've chosen to organize your project maintaining
`rules`_ to enforce specific CI behaviors on different GitLab instances
may not be supportable. In this case it would be easier to manage
completely separate pipelines for each GitLab instance. This is achievable
since the CI configuration file (default being ``.gitlab-ci.yml``) is
configurable on a project basis. To do so simply navigate to **Settings** -->
**CI/CD** --> **General pipelines**:

.. image:: ../tutorial/files/gitlab_ci_timeout.png
    :scale: 75%

By defining a new **Custom CI configuration path** on each GitLab instance
we can manage facility specific configurations. Looking at the contents
of an example project:

    .. code-block:: console

        .gitlab\               # Shared CI configuration.
        .gitlab-ci.yml         # Configuration for GitLab.com.
        alcf.gitlab-ci.yml     # Configuration for ALCF's GitLab instance.
        nersc.gitlab-ci.yml    # Configuration for NERSC's GitLab instance.

Since the configuration is done on the project level, all code development
and mirroring can be done without having to juggle aligning CI functionality
across all instances.
