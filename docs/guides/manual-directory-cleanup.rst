Manual Directory Cleanup
========================

The goal of this example is to quickly demonstrate a potential workflow
that will provide teams with the ability to run potential cleanup tasks
when someone on the team `manually <https://docs.gitlab.com/ee/ci/yaml/#whenmanual>`_
initiates the associated job. This may be a desirable
workflow for teams that generate substantial data/caches that they use
underlying facility managed file systems to store. With the **setuid**
enhancements enabled, the manual job will always run under the user who
triggered it via the web.

.. warning::

    Removing files from a system as part of your CI job can be dangerous.
    Take care to verify the target(s) for removal either manually or
    with scripts.

.. literalinclude:: ../../source/manual-directory-cleanup/.gitlab-ci.generic.yml
    :language: yaml

.. image:: files/manual-remove-generic.png
    :scale: 60%

There are other GitLab supported tools one can leverage
in such as workflow. We recommend checking out the upstream post on
`protecting manual jobs <https://about.gitlab.com/blog/2020/02/20/protecting-manual-jobs/>`_
for how access can be limited to manual gates in your CI pipeline.

Though we've chosen to highlight simply `manually`_
removing files/folders, it is not the only option available to teams. In some
cases it may make more sense to develop your own script(s) and leverage
GitLab's `rules <https://docs.gitlab.com/ee/ci/yaml/#rules>`_ as well as
`scheduling <https://docs.gitlab.com/ee/user/project/pipelines/schedules.html>`_
to automate desired cleanup.

You may also wish to make cleanup a job-level
action by creating an `after_script <https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script>`_;
however, it should be noted that canceled jobs end abruptly preventing
not just subsequent stages from running but also the job itself.
Please refer to official GitLab efforts to
`ensure after_script is called for cancelled and timed out pipelines <https://gitlab.com/gitlab-org/gitlab/-/issues/15603>`_
for the status of a fix.
