Setuid Feature
==============

.. important::

   All further development efforts have been shifted to supporting
   our implementation of a
   `custom executor driver (Jacamar CI) <https://gitlab.com/ecp-ci/jacamar-ci>`_,
   as opposed of further enhancing
   the complete `fork <https://gitlbab.com/ecp-ci/gitlab-runner>`_
   of the upstream runner. We are committed to continuing support
   for all ECP enhancements in this greatly improved CI model. All feedback
   is welcome as we continue to work towards a production ready release.

Overview
--------

By default, all GitLab CI jobs run as the single gitlab-runner user. If you
require isolation between CI jobs on your runner hosts (e.g., for
security on a multi-tenant system), enhanced runner's setuid functionality
should be utilized. Setuid functionality ensures that
`GitLab's security model <https://docs.gitlab.com/ee/user/permissions.html>`_
is enforced consistently, all the way from the GitLab server to the runner
hosts. Multiple GitLab users can share a single setuid runner securely, without
the risk of one user accessing another's data.

While GitLab does its best to preserve isolation between users on the server
(e.g., one user cannot access another user's repository without permission).
The default runner configuration does not enforce this same  isolation. If CI
jobs from different GitLab users are scheduled on the same runner, one
job can access any data the other job stores on the runner host's
filesystem. This is insecure.

Let's examines several use cases where a runner with setuid functionality
would be required:

1. *Basic runner data isolation*: One user's GitLab CI job cannot access
   the working directory or cached data of another user's CI job. This is
   provided with setuid functionality by ensuring that each job's working
   directory and cache data directory are owned and only accessible by
   the user who initiated the job. With setuid enabled, all CI processes
   will run as the job initiator, ensuring file permissions are enforced.

#. *Account-specific resources*: On some systems, certain resources and
   privileges are associated with specific user accounts. For example,
   at nearly all high performance computing facilities, CPU time
   allocations are associated with user accounts. CI jobs must run as
   specific users on the runner host in order to submit compute jobs to
   the batch system. Setuid functionality ensure that users can access these
   resources in addition to ensuring allocations are properly observed.

#. *Shared, sensitive data sets*: At many sites, sensitive applications
   and data sets are deployed in a shared filesystem on multi-tenant
   systems, with access restricted to certain users or unix groups. Just
   as setuid jobs ensure that a runner's working directory and cache data
   are protected, they also ensure that OS-level permissions are
   respected by CI jobs for shared data sets.

Preliminary security considerations
-----------------------------------

*Root privileges*: Runners using SetUID need privileges to start
processes as other users.  If you can't launch a process as root on a
host, you can't use a SetUID runner on that host.

*Trust*: The SetUID implementation currently assumes that the GitLab
server and the hosts of any SetUID runners trust each other, and that
the server administrators are the same as runner administrators. This
typically means that both GitLab and the host OS of runner hosts
should use the same identity management system for user accounts. The
typical way to ensure this is to configure GitLab and runners to use the
same directory server (LDAP, Active Directory, etc.). This is a very
common setup for on-premise GitLab installations at large sites.

Barring that, at a minimum, if a user `alice` exists on the GitLab
system, runner administrators must ensure that an `alice` user also
exists on the runner host, and that both `alice` users are the same
person. If `alice` exists in GitLab but not on a runner host, CI jobs
initiated by alice will fail when sent to that runner.

*HTTPS*: Because SetUID runners are trusted, you should ensure that
communication between the server and the runner is secured using
HTTPS. SetUID runners run as root, and if an attacker can impersonate the
GitLab server, they can compromise your runner hosts.

Security Model
--------------

The SetUID functionality relies on the
`CI job permissions model <https://docs.gitlab.com/ee/user/project/new_ci_build_permissions_model.html>`_
that was introduced in GitLab 8.12. In particular, the following points
from the CI job permission spec are key to understand:

1. Job permissions should be tightly integrated with the permissions of a
   user who is triggering a job;
#. It is simple and convenient that your job can access everything that
   you as a user have access to;
#. We (in this case, runner host machines) already know what the user is
   allowed to do;
#. Any job that is triggered by the user, is also marked by GitLab with
   their permissions;
#. GitLab already fully knows who is triggering a job.

The setuid feature extends these principles from GitLab into the runner
host by executing CI jobs as their GitLab users on the runner host.
Since CI jobs now run as unprivileged user processes, job permissions
carry over from GitLab to the OS (1, 4), and they can access files and
directories in the host filesystem that their user has access to (2,
3). The GitLab server is still ultimately responsible for determining a
job's permissions (4, 5), but instead of simply issuing the job tokens,
the server also tells the setuid runners who to run as.

`Initiators`: There are many ways to initiate a CI Job in GitLab, and
GitLab will mark a job with the permissions of the initiating user. In
setuid runners, CI jobs will run as this user, so it is important to
be aware of who the initiator is in each case:

* `git push`: jobs run as the pusher.
* `Creating a PR`: jobs run as the creator of the PR.
* `Pull Mirroring and GitHub Integration`: jobs run as the mirror creator.
* `Scheduled pipelines`: jobs run as the schedule owner.
* `Webhook triggers`: jobs run as the triggering user.

Setuid runner configuration
---------------------------

.. code-block:: toml

  [[runners]]
  setuid = true

Using any aspect of *setuid* in the enhanced runner will first require you to
enable it via the ``setuid`` option as a runner level. From there you will
also be able to manage other aspects of the authorization process related to
the concept of tieing the GitLab user to a valid local account.

Setuid Data Dir
~~~~~~~~~~~~~~~

.. code-block:: toml

  [[runners]]
  setuid_data_dir = "/ecp/gitlab-runner"

Upstream runner supports the *build_dir* and *cache_dir*
`advanced configuration options <https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section>`_.
Though with these runner options you can dictate where the related job data
is stored you are unable to secure it in a multi-tenant environment. As part
of the Setuid enhancements you are able to define a single location that the
runner will manage for all build direction as well as local caching.

When defined, the runner will create the appropriate directory structure
(seen below) and enforce security by creating a final directory owned by
the authorized user and *0700* file permissions. All files/folders
created automatically as part of the CI job will remain within this
secured structure.

.. code-block:: console

  $ namei -l $(pwd)
    dr-xr-xr-x root    root    /
    drwx-----x root    root    data-dir
    drwx-----x root    root    group
    drwx-----x root    root    project
    drwx-----x root    root    builds
    drwx-----x root    root    users
    drwx------ user    user    username

There is also the option to define ``setuid_data_dir = "$HOME"``, this will
leverage the local user's home directory.

Blacklist and Whitelist
~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: toml

  [[runners]]
  setuid_user_whitelist = [
      "alice",
      "david",
  ]
  setuid_user_blacklist = ["paul"]
  setuid_group_whitelist = ["ecp"]

Providing further control to which users and/or groups have access to the
the runner, four additional configuration options where added. Each supporting
and array of associated strings that the runner will use to validate each job.
During the SetUID preparation phase of a job the details and group members of
the potential user are identified ont he local systems then those details
are validated against the configured lists. If a failure state is encountered
the job will fail before any user influenced code is executed. An error message
for this failure is both logged via the runner as well as displayed to the
user.

- ``setuid_user_whitelist`` - An authoritative list of users who can execute
  Gitlab SetUID Runners.
- ``setuid_user_blacklist`` - A list of usernames that are not allowed to run
  CI jobs. More authoritative than group whitelist / blacklist, but can be
  overridden by SetUIDUserWhitelist.
- ``setuid_groups_blacklist`` - A list of groups that are not allowed to run
  CI jobs.
- ``setuid_groups_whitelist`` - A list of groups that are allowed to run CI
  jobs. Least authoritative.

It is important to keep in mind when using lists that first the
``setuid: true`` must be configured leastwise the configurations will be
ignored. Additionally any usernames
or groups provided are assumed to relate to Linux user/groups on the local
system. The runner **does not** ensure the lists configured in the
``config.toml`` are in fact valid.

As of runner release *0.5.5* the `Shell Whitelist`_ functionality has been
introduced. This also occurs during this phase of the authorization flow.

User Whitelist
""""""""""""""

The ``setuid_user_whitelist`` is a list of string usernames that will always be
allowed to run CI jobs. This is the highest precedence list.

User Blacklist
""""""""""""""

The ``setuid_user_blacklist`` is a list of string usernames that will not be
permitted to run CI jobs. If a username is on both the whitelist and the
blacklist, the user will be permitted to run jobs.

Group Blacklist
"""""""""""""""

The ``setuid_groups_blacklist`` is a list of string group names that will not
be permitted to run CI jobs. If a user attempts to run a job and that user is
a member of **any** of the groups in the groups blacklist, their job will fail
with an error that indicates which groups they are a part of that are on the
blacklist.

- User whitelist and blacklist take precedence over the groups whitelist and
  blacklist. This enables sites to allow or disallow entire groups while
  making exceptions for individual users.
- The groups blacklist is higher precedence than the groups whitelist. This is
  slightly irregular, but due to the many-to-many relationship of groups that
  a user can be assigned to and groups that may be on the blacklist, it is
  safer to deny access to a user that is on both the groups whitelist and the
  groups blacklist than it is to permit access.

Group Whitelist
"""""""""""""""

Finally, the ``setuid_groups_whitelist`` is a list of string group names that
will be permitted to run CI jobs. If a user is a member of any group that is on
this list, and is also neither in a group that is on the blacklist nor on the
user blacklist, they will be permitted to run CI jobs.

- If this key is not defined, and a user does not fall into any other whitelist
  or blacklist, then that user will be allowed to run jobs. If no whitelists
  or blacklists are defined, then all users will be allowed to run jobs.
- If a user falls into both this list and a blacklist, they will not be allowed
  to run jobs.
- If a groups whitelist is defined but a user does not belong to a group on the
  whitelist, that user will not be allowed to run CI jobs. Again, defining a
  groups whitelist implies that a user that is **not** on that whitelist
  should **not** be permitted to run CI jobs. If there is no groups whitelist
  defined, **all** users will be permitted run CI jobs, barring the effects
  of other whitelists / blacklists.

Shell Whitelist
~~~~~~~~~~~~~~~

:sub:`Added in runner release 0.6.0`

.. code-block:: toml

  [[runners]]
    shell_whitelist = ["/bin/bash", "/bin/zsh"]

During the `Blacklist and Whitelist`_ checks it is now possible to verify the
target setuid user's shell. If a list (``shell_whitelist``) is defined the
local user account's default shell is obtained then validated. If the user's
shell is not found in the list the CI job will fail before the setuid process
has completed. Please observe that the check will occur both for the local
validated GitLab user in addition to the target service account (if
``setuid_allow_run_as = true``).

Due to upstream CI tests on the source code, we've elected to use
`getent(1) <http://man7.org/linux/man-pages/man1/getent.1.html>`_ to obtain
the relevant entry from the database as opposed to a more traditional
method involving `getpwuid(3) <https://linux.die.net/man/3/getpwuid>`_. As this
is currently an optional configuration that we will further expand
with feedback.
