HPC GitLab-Runner 13.0.0~hpc.0.6.1
==================================

* *Release Date*: 6/23/2020
* *Commit*: db78675d
* *Tag*: `v13.0.0-hpc.0.6.1 <https://gitlab.com/ecp-ci/gitlab-runner/-/tags/v13.0.0-hpc.0.6.1>`_

There are no HPC focused changes (e.g. batch or setuid) in
this release. As such the `golang-hpc <https://gitlab.com/ecp-ci/golang-hpc>`_
version remains the same and the release RPM will be name as
*gitlab-runner-0.6.1-2.<arch>.rpm*. The RPM may not be required for any
currently supported test deployments and changes should be examined as
they mainly target federation testing/acceptance efforts.

.. note::

  Unless there are unexpected circumstances this will be the last full
  release of the HPC enhanced fork. Future releases will leverage
  GitLab's custom executor model with a substantially diminished
  number of source changes required.

Admin Changes
-------------

* The upstream GitLab Runner
  `version 13.0 <https://gitlab.com/gitlab-org/gitlab-runner/blob/v13.0.0/CHANGELOG.md>`_
  changes have been merged into the fork. Previously based off version 12.7.

* RPMs generated will no longer attempt to create a runner service upon
  initial installation.

  - You can choose to add the service manually, see:
    ``gitlab-runner install --help``

*  Introduces support for the ``job_env`` object to the custom executor's
   expected JSON configuration payload. The key-value pairs identified
   in this object will be made available to all subsequent stages within
   that CI job.

  - This is only used to assist with testing of the ECP custom executor driver
    (Jacamar). Future releases should see this upstream.

* Added check for duplicate variables to the custom executor.

* Federation enhancements target ECP and ECP-Dev instances.
