HPC GitLab-Runner 12.6.0~hpc.0.5.3
==================================

* *Release Date*: 1/16/2020
* *Commit*: b58ff3d4
* *Tag*: `v12.6.0-hpc.0.5.3 <https://gitlab.com/ecp-ci/gitlab-runner/-/tags/v12.4.0-hpc.0.5.3>`_

General Changes
---------------

* The runner has been rebased to
  `version 12.6.0 <https://gitlab.com/gitlab-org/gitlab-runner/blob/v12.6.0/CHANGELOG.md>`_
  of the upstream GitLab Runner, previously ``12.4.0``.

* In an effort to support runner on potentially remote systems the runner's
  local date/time
  will be displayed upon the start of the job (via web user interface).

  .. code-block:: console

    Running with gitlab-runner 12.6.0~hpc.0.5.3
        on Example Runner
        job starting 2020-01-16 14:28:30
    Using SetUID batch executor
    Running as ...

Admin Changes
-------------

*  Slurm will now rely only on using ``sacct`` to poll job instead of
   ``squeue``.

   - ``sacct`` has less overhead on the Slurm controlled for polling.

   - We also add additional flags such as ``-L`` to handle jobs
     from different clusters as well a the ``-S`` flag for choosing
     he start date in the event that a cluster has a similar job id
     but from an older date.

* In order to better support facility requirements changes have been
  made to the RPM creation process. Upstream GitLab uses
  `FPM <https://github.com/jordansissel/fpm>`_ to create the packages;
  however, in our case we will follow RedHat's documentation as the
  required supported systems is much smaller. Several major changes
  include:

  - Naming convention for RPM has changed (please note as this may
    cause issue in initial upgrade).

  - There is no longer the creation of a ``gitlab-runner`` account
    automatically. It is up to system administrators to decide on
    the need for a runner specific service account.

  - All post installation scripts will no longer attempt to
    interact with Docker.

  - There are no longer scripts included (e.g. post-install), they
    are accounted for correctly in the spec file.

  - Upgrading will no longer remove any systemd files (e.g.
    ``/etc/systemd/system/gitlab-runner.service``). Please note this
    is only during upgrades. Uninstall and then installing a runner
    will still cause this to occur.

  - Excluding the above changes the aim was to keep in line with the
    traditional runner installation.
