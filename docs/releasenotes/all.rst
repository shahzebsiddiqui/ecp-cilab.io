All Releases
============

Detailed release notes for all runner and server development efforts. Due to
the nature of managing multiple forks as well as distinct HPC focused libraries
centralizing notes hopefully allows for easier review as well as
clearer documentation of the changes for both administrators and users alike.

.. toctree::
   :maxdepth: 1
   :caption: Jacamar CI

   jacamar/jacamar_0.1.0.rst

.. toctree::
   :maxdepth: 1
   :caption: Runner (fork w/ECP Enhancements)

   runner/runner_13.0.0_0.6.1.rst
   runner/runner_12.7.0_0.6.1.rst
   runner/runner_12.7.0_0.6.0.rst
   runner/runner_12.6.0_0.5.4.rst
   runner/runner_12.6.0_0.5.3.rst
   runner/runner_12.4.0_0.5.2.rst
   runner/runner_12.4.0_0.5.1.rst
   runner/runner_12.4.0_0.5.0.rst
   runner/runner_12.0.0_0.4.3.rst
