ECP CI - Quick Start Tutorial
=============================

.. _HPC focused enhancements: ../introduction.html#enhancements
.. _GitLab: https://about.gitlab.com/
.. _Batch executor: ../customers/ci-batch.html

The goal of this tutorial is to provide a high-level introduction to GitLab
CI/CD that helps individuals get started in 30 minutes and provide a basic
understanding of the `HPC focused enhancements`_. Though geared toward
beginners this documentation does assume a basic familiarity with GitLab_.

If you are unfamiliar with GitLab or would like to start
by developing a deeper understanding of the tools there are
several other resources we can recommend:

* The official `getting started with CI/CD <https://docs.gitlab.com/ee/ci/quick_start/README.html>`_
  guide is a great resource. It also has the the added bonus it can be
  completed with resources freely available on `GitLab.com <https://gitlab.com>`_.
* We've created a more in `depth series of tutorials <../customers.html#Tutorials>`_
  that have been modified to relate to the specific site local
  resources, with additional guides under development.

Introduction
------------

The ECP Software Deployment Team is working towards enabling both cross site
as well as internal continuous integration (CI) for approved code teams. When
fully operational your code can be compiled and tested with greatly minimized
interactions using a central GitLab instance. There are also many internal
instances of GitLab that are available to use today, with access to an
internally managed set of resources. This guide provides all
the necessary details to create a MPI "hello world" example that will be
ran in a CI pipeline. There are both generalized as well as site specific
details include in order to best support wherever you attempt this tutorial.

Why GitLab CI/CD?
-----------------

CI/CD is short for Continuous Integration / Continuous Delivery or Continuous
Deployment. It can provide teams a configurable, automated, and
repeatable process to build/test their code. Though there are a number of
available CI/CD solutions the ECP Software and Facility teams have
chosen to use `GitLab CI/CD <https://docs.gitlab.com/ee/ci/>`_. We highly
recommend that if you encounter any issues or detailed questions during this
tutorial to check the `official documentation <https://docs.gitlab.com/>`_.
We will take special care to highlight changes directly associated with
running on facility resources as these won't be reflected in other
documentation sources.

Basics of GitLab CI/CD
~~~~~~~~~~~~~~~~~~~~~~

In this Quick-Start Guide, we use a simple “hello, world” example, with MPI
and MPI+OpenMP C codes. Our repository will need to have the
required source code to build, the same as any other repository.
The difference is that we must also define a ``.gitlab-ci.yml``
file. This file acts as the instructions for GitLab to build and execute
your CI pipeline.

The steps that the pipelines execute are called jobs. When you group a series
of jobs by those characteristics it is called stages. Jobs are the basic
building block for pipelines. They can be grouped together in stages and stages
can be grouped together into pipelines.

.. image:: files/quickstart/example_pipeline.png
   :scale: 50%

In our example pipeline we are going to focus on create two stages; *build*
and then *test*. The *build* stage will, as the name implies, build
our MPI application using an available software environment wherever my
job is executed. While in the *test* stage we will request compute
resources to execute our newly created binaries using the `Batch executor`_.
Our example is incredibly simply and depending on the scale/complexities
of your planned CI/CD process there are wealth of `features <https://docs.gitlab.com/ee/ci/yaml/>`_
you can decide to leverage we simply don't touch upon in this tutorial.

Getting Started
---------------

To get started you will require access to a GitLab repository as well as a
GitLab runner that has been registered with that instance.
If you have access to a facility GitLab instance than you should be ready
to go. Begin by
`creating a new project <https://docs.gitlab.com/ee/gitlab-basics/create-project.html>`_.
The name doesn't matter and since this is a test project
and you can leave the visibility to private. From this point it
is possible to manage files using a tradition Git workflow
but alternatively you can also choose to use the GitLab
`Web IDE <https://docs.gitlab.com/ee/user/project/web_ide/>`_.

Setting up your repository
--------------------------

Now that you have an empty project you will need to create three separate
files.

**hello_mpi.c**

.. literalinclude:: ../../source/gitlab-quickstart/hello_mpi.c
    :language: c

**hello_mpi_omp.c**

.. literalinclude:: ../../source/gitlab-quickstart/hello_mpi_omp.c
    :language: c

**Makefile**

.. literalinclude:: ../../source/gitlab-quickstart/makefile

Writing you CI YAML File
------------------------

To use GitLab CI/CD, create a file named ``.gitlab-ci.yml`` at the root of the
project repository. Though the majority of the file will remain consistent
across facilities, we have attempted to highlight differences that most
observed depending one where your running.

  * *Generic* - Uses an imaginary instance running Slurm.
  * *ALCF* - Written to run on Theta test resources.

.. note::

    Take care while writing the `YAML <https://yaml.org/>`_ to avoid tabs and
    instead use spaces for indentation.

First, each job must be assigned a
`stage <https://docs.gitlab.com/ee/ci/yaml/#stages>`_. The naming of the
stages is arbitrary. In our example, we wish to have two stages
with the names ``build`` and ``test``.

.. code-block:: yaml

  stages:
    - build
    - test

Next we are going to define several variables that will be required to use
the `Batch executor`_. Variables are an important concept in GitLab CI, each job
is provided a prepared build environment that incorporates both
`project defined <https://docs.gitlab.com/ee/ci/variables/>`_ as well as system
`predefined variables <https://docs.gitlab.com/ee/ci/variables/predefined_variables.html>`_.

.. tabs::

    .. tab:: Generic

        In this example we are interacting with Slurm, requesting an allocation through
        `sbatch <https://slurm.schedmd.com/sbatch.html>`_. The variable
        ``SCHEDULER_PARAMETERS`` is the default variable the Batch executor will look
        for when attempting to identify command line arguments to pass to the underlying
        scheduler.

        .. code-block:: yaml

            variables:
              SCHEDULER_PARAMETERS:  "-N 1 -n 64 -p ecp-p9-4v100 -t 0:03:00"

    .. tab:: ALCF

        When using runners at ALCF you will be interacting with Cobalt, specifically
        `qsub <https://trac.mcs.anl.gov/projects/cobalt/wiki/qsub.1.html>`_. Administrators
        have specified that the ``ANL_THETA_SCHEDULER_PARAMETERS`` should be used to define
        command line argument you wish to supply to the underlying scheduler. While the
        ``ANL_THETA_PROJECT_SERVICE_USER`` is required to specify your project specific
        service user you wish to request access too. Please note, you **must**
        have already been granted access to use this account or else CI will fail during
        its authorization phase.

        .. code-block:: yaml

            variables:
              ANL_THETA_PROJECT_SERVICE_USER: "<serviceAccount>"
              ANL_THETA_SCHEDULER_PARAMETERS: "-A <accounting> -n 1 -t 3 -q debug-flat-quad"

Next we want to define several hidden keys that we can later refer to with
GitLab `extend <https://docs.gitlab.com/ee/ci/yaml/#extends>`_ keyword. By
doing so it becomes possible to easily reuse elements of your YAML file.
In this case we want to define a series of `tags <https://docs.gitlab.com/ee/ci/yaml/#tags>`_,
used to specify the runner we wish our job to execute on. Additionally, we
will define `rules <https://docs.gitlab.com/ee/ci/yaml/#rules>`_ to prevent
unnecessarily using our compute resources to test. With this rule defined
the job's that inherit from this key will only when if they are started
either manually via the web interface or a
`pipeline schedule <https://docs.gitlab.com/ee/ci/pipelines/schedules.html>`_.

.. tabs::

    .. tab:: Generic

        .. code-block:: yaml

          .shell-runner:
            tags: [shell, ecp-ci]

          .batch-runner:
            tags: [slurm, ecp-ci]
            rules:
            - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedule"'
              when: always


    .. tab:: ALCF

        .. code-block:: yaml

          .shell-runner:
            tags: [shell, anl-ci, ecp-theta]

          .batch-runner:
            tags: [batch, anl-ci, ecp-theta]
            rules:
            - if: '$CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "schedule"'
              when: always

We can also define a pipeline wide `before_script <https://docs.gitlab.com/ee/ci/yaml/#before_script-and-after_script>`_
that will be observed by every job we create. In our case lets use it to load
the necessary software modules.

.. tabs::

    .. tab:: Generic

        In order to build the soft ware we will first need to load software modules
        available on the underlying system. In our example Slurm environment
        that is simply a **gcc** and **openmpi** module.

        .. code-block:: yaml

          before_script:
            - ml gcc/8.3.0 openmpi/3.1.4

    .. tab:: ALCF

        For the ALCF environment running on Theta we will need to load
        the **PrgEnv-gnu** module.

        .. code-block:: yaml

          before_script:
            - module load PrgEnv-gnu

Now we can define a job for the *build* stage. The goal is make both the MPI
as well as MPI + OMP binaries. We are going to `extend`_ the hidden keys we
just defined and also introduce the concept of
`artifacts <https://docs.gitlab.com/ee/ci/yaml/#artifacts>`_. Using
artifacts GitLab will upload the defined paths to the server and all jobs
in subsequent stages will then download these. If we did not use some
mechanism to capture the binaries they would be deleted whenever the next job
started. It is also important to note that the ``script`` defined at the job
level will always be executed with `Bash <https://docs.gitlab.com/runner/shells/>`_
when using facility runners.

.. tabs::

    .. tab:: Generic

        .. code-block:: yaml

          mpi-build:
            stage: build
            extends:
              - .shell-runner
            script:
              # Obverse the directory structure.
              - ls -la
              - make hello_mpi_exe
              - make hello_mpi_omp_exe
            artifacts:
              paths:
                - ./hello_mpi_exe
                - ./hello_mpi_omp_exe

    .. tab:: ALCF

        .. code-block:: yaml

          mpi-build:
            stage: build
            extends:
              - .shell-runner
            script:
              # Obverse the directory structure.
              - ls -la
              - make hello_mpi_exe CC=cc
              - make hello_mpi_omp_exe CC=cc
            artifacts:
              paths:
                - ./hello_mpi_exe
                - ./hello_mpi_omp_exe

Finally we can create the job for our *test* stage. In this case we will be
using the runner that is configured as a `Batch executor`_ and as such the
script will be submitted to the underlying scheduling system using our
earlier defined parameters. Keep in mind that depending on where you
are running you will need to account for differences in executing your
tests on the compute resources.

We are also choosing to specify variables at the job level. Earlier
in the file we specified them at a pipeline level, there is a
`priority of environment variables <https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables>`_
that will be observed.

.. tabs::

    .. tab:: Generic

        .. code-block:: yaml

            mpi-test:
              stage: test
              extends:
                - .batch-runner
              variables:
                MPI_TASKS: 64
                OMP_NUM_THREADS: 2
              script:
                - srun -n ${MPI_TASKS} ./hello_mpi_exe | sort -n -k9
                - mpirun -n ${MPI_TASKS} ./hello_mpi_omp_exe | sort -n -k9

    .. tab:: ALCF

        .. code-block:: yaml

            mpi-test:
              stage: test
              extends:
                - .batch-runner
              variables:
                MPI_TASKS: 64
              script:
                - aprun -n ${MPI_TASKS} -N ${MPI_TASKS} -cc depth -d 4 -j 4 ./hello_mpi_exe | sort -n -k9
                - aprun -n 1 -N 1 -d 8 -j 1 -cc depth -e OMP_NUM_THREADS=8 ./hello_mpi_omp_exe | sort -n -k9

Now that you have defined your ``.gitlab-ci.yml`` file it is time to run it.
Since we've defined rules_ for *test* stage we know that it will not run
without some manual intervention. In this case simply go to
**CI/CD -> Pipelines** and select **Run Pipeline**.

.. image:: files/quickstart/run_pipeline.png
    :scale: 30%

Viewing the Results
-------------------

Now by returning to the **CI/CD -> Pipelines** screen you will be able to
view your completed (or running) pipeline.

.. image:: files/quickstart/pipeline_results.png
   :scale: 30%

Selecting a job from that pipeline will provide you with a detailed look,
including all *stdout*/*stderr* generated. This screen will be updated
constantly throughout the job but beware there will be a noticeable delay.

.. image:: files/quickstart/job_results.png
   :scale: 30%

Completed YAML
~~~~~~~~~~~~~~

.. tabs::

  .. tab:: Generic

    .. literalinclude:: ../../source/gitlab-quickstart/.gitlab-ci.generic.yml
      :language: yaml

  .. tab:: ALCF

    .. literalinclude:: ../../source/gitlab-quickstart/.gitlab-ci.alcf.yml
      :language: yaml
