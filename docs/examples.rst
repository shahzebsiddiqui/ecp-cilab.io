Example Projects and Guides
===========================

.. note::

   We are always actively looking to expand this section with new site
   specific examples and beneficial guides. Efforts to improve these are
   heavily influence by your feedback and questions, so please do not
   hesitate to reach out.

Examples
--------

Example projects have been created and verified with the HPC enhanced executor
to provide not only recommended practices but also an interactive avenue to
better understand GitLab CI. Even if you already have experience with the
tools we've taken the time to also highlight HPC
focused examples that can assist you and your team.

.. list-table::
    :header-rows: 1
    :widths: 20, 4, 4, 4, 4, 40

    * - Title
      - ALCF
      - OLCF
      - NERSC
      - GitLab
      - Description
    * - `GitLab CI Basics <examples/gitlab-ci-basics.html>`_
      - √
      - √
      - x
      - √
      - A simple multi-stage "Hello World" build/test pipeline.

Guides
------

Guides have been written to highlight both best practices as well as
potential workflows you may want to leverage when using both facility
and ECP CI resources.

.. toctree::
   :maxdepth: 1

   guides/manual-directory-cleanup.rst
   guides/build-status-gitlab.rst
   guides/multi-gitlab-project.rst
   guides/hpc-artifacts-caching.rst
